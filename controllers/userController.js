const User = require("../models/User.js");
const Course = require("../models/Course.js");
const auth = require("../auth.js");

// need natin import yung ginawang user model kanina sa Users.js file
const bcrypt = require("bcrypt");


// registration
module.exports.registerUser=(reqBody)=>{
	let newUser= new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNumber: reqBody.mobileNumber,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// authentication
module.exports.loginUser=(reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result =>{
		// if the user email does not exist:
		if (result===null){
			return false;
		// if the user email exists in the DB:
		}else{
			const isPasswordCorrect=bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access:auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}
// check if email exists
module.exports.checkEmailExists=(reqBody)=>{
	return User.find({email: reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
			// meron existing email kaya true
		}else{
			return false;
		}
	})
}

// get all users
module.exports.getAllUsers=()=>{
	return User.find({}).then(result=>{return result
	})
};

module.exports.getProfile=(data)=>{
	console.log(data);
	return User.findById(data.userId).then(result=>{
		// Changes the value of the user's password to an empty string when returned to the frontend. not doing so will expose the user's password which will also not be needed in other parts of the applic
		// unlike in register method
		result.password="";
		return result
	});
};

// set user as admin
module.exports.setAsAdmin = (reqParams, reqBody) => {
	let updateAdminField = {
		isAdmin : true
	};
	return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};



// enroll in a course
module.exports.enroll=async(data)=>{
	
	let isUserUpdated=await User.findById(data.userId).then(user=>{
		user.enrollments.push({courseId: data.courseId});
		// saves the updated user info in the DB
		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	let isCourseUpdated=await Course.findById(data.courseId).then(course=>{
	
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	});
	
	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{
		return false;
	}
}

// get all enrollees
module.exports.getAllEnrollees=({})=>{
	return User.find({status: "Enrolled"}).then(result=>{return result
	});
};

// get own orders of users
module.exports.getMyCourses=(reqBody)=>{
	return User.find({status: "Enrolled"}).then(result=>{return result
	});
};
