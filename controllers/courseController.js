const Course=require("../models/Course.js");

module.exports.addCourse = (data) => {
		let newCourse = new Course({
			name : data.name,
			description : data.description,
			duration: data.duration,
			price : data.price
		});
		return newCourse.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
};


// retrieve all courses
/*
retrieve all courses from the database and return the result
*/
module.exports.getAllCourses=()=>{
	return Course.find({}).then(result=>{return result
	})
};

module.exports.getAllActive=()=>{
	return Course.find({isActive:true}).then(result=>{return result;
	})
};

// retrieving a specific course by ID
module.exports.getCourse=(reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{return result;
	})
};


// update a course
module.exports.updateCourse=(reqParams, reqBody)=>{
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		duration: reqBody.duration,
		price: reqBody.price
	}
	
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, err)=>{
		if (err){
			return false;
		} else{
			return true; 
		}
	})
};

// retrieving course by ID
module.exports.getCourse=(reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{return result;
	})
};


// delete/archive
module.exports.archiveCourse = (reqParams, reqBody) => {
	let updateActiveField = {
		isActive : false
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};



