const express = require("express");
const router=express.Router();
const userController=require("../controllers/userController.js");
const auth=require("../auth.js");

// Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

// route for user registration
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})


// route for user authentication
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});

// route to get all users
router.get('/all', (req, res)=>{
	userController.getAllUsers().then(resultFromController=>res.send(resultFromController));
});

// route for retrieving user details 
router.get("/details", auth.verify, (req,res)=>{
	// 
	const userData=auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});

// route to set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin===false){res.send(false);
	}else{
	userController.setAsAdmin(req.params).then(resultFromController=>res.send(resultFromController));
	}
});


router.post('/enroll', auth.verify, (req, res)=>{
		let data={
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController=>res.send(resultFromController));
});


// route for retrieving enrollees 
router.get("/enroll", auth.verify, (req,res)=>{
	// 
	const userData=auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getAllEnrollees({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});

// router.get('/enroll', auth.verify, (req, res)=>{
// 		let data={
// 			userId: auth.decode(req.headers.authorization).id,
// 			courseId: req.body.courseId

// 		}
// 		userController.enroll(data).then(resultFromController=>res.send(resultFromController));
// });


// route to get courses enrolled in by a user
router.get('/myCourses', auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin===true){res.send(false);
	}else{
	userController.getMyCourses({status: "Enrolled"}).then(resultFromController=>res.send(resultFromController));
	}	
});


module.exports=router;