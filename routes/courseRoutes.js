const express = require("express");
const router=express.Router();
const courseController=require("../controllers/courseController.js");
const auth = require("../auth.js");


// Route for creating a course (no admin verification)
router.post("/", (req,res)=>{
	courseController.addCourse(req.body).then(resultFromController=>res.send(resultFromController));
});

// Route for creating a course w/admin verification
router.post("/", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	}
});


// route for getting all courses
router.get('/all', (req, res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController));
});


// route for getting all ACTIVE courses
router.get('/active', (req, res)=>{
	courseController.getAllActive().then(resultFromController=>res.send(resultFromController));
});

// route for retrieving a specific course
router.get('/:courseId', (req,res)=>{
	courseController.getCourse(req.params).then(resultFromController=>res.send(resultFromController))
});

// updating a course
router.put('/:courseId', auth.verify, (req, res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController));
});

// archive a course)
router.put("/:courseId/archive", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin===false){res.send(false);
	}else{
	courseController.archiveCourse(req.params).then(resultFromController=>res.send(resultFromController));
	}
});



module.exports= router;