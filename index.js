const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

// routes
const userRoutes=require("./routes/userRoutes.js");
const courseRoutes=require("./routes/courseRoutes.js");

const app = express();

mongoose.connect("mongodb+srv://jpunzalan:iloveNZ@wdc028-course-booking.0tib1.mongodb.net/Pera-Paraan?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=>console.log("we're connected to the database"));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


app.listen(process.env.PORT||4000, ()=>{console.log(`API now online at port ${process.env.PORT||4000}`)});
